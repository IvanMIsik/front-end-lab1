function task1() {
    document.getElementById('byrgeri').value = parseInt((document.getElementById('groshi').value) / (document.getElementById('vartist').value));
    document.getElementById('reshta').value = parseInt((document.getElementById('groshi').value) - (document.getElementById('byrgeri').value) * (document.getElementById('vartist').value));
}
function task2() {
    let last = document.getElementById('chislo').value % 10 * 10000;
    document.getElementById('res').value = parseInt(document.getElementById('chislo').value / 10) + last;
}
function task3() {
    var prev = new Date(document.getElementById('date').value);
    prev.setTime(prev.getTime() - 24 * 60 * 60 * 1000);
    prevDate = prev.getDate();
    prevMonth = (prev.getMonth() + 1);
    if (prevDate < 10) prevDate = '0' + prevDate;
    if (prevMonth < 10) prevMonth = '0' + prevMonth;
    document.getElementById('prev').value = prevDate + "." + prevMonth + "." + prev.getFullYear();
    var next = new Date(document.getElementById('date').value);
    next.setTime(next.getTime() + 24 * 60 * 60 * 1000);
    nextDate = next.getDate();
    nextMonth = (next.getMonth() + 1);
    if (nextDate < 10) nextDate = '0' + nextDate;
    if (nextMonth < 10) nextMonth = '0' + nextMonth;
    document.getElementById('next').value = nextDate + "." + nextMonth + "." + next.getFullYear();
}
function task4() {
    let yearsInWords;
    if (document.getElementById('roku').value < 10) {
        switch (parseInt(document.getElementById('roku').value)) {
            case 1:
                yearsInWords = "один";
                break;
            case 2:
                yearsInWords = "два";
                break;
            case 3:
                yearsInWords = "три";
                break;
            case 4:
                yearsInWords = "чотири";
                break;
            case 5:
                yearsInWords = "п'ять";
                break;
            case 6:
                yearsInWords = "шість";
                break;
            case 7:
                yearsInWords = "сім";
                break;
            case 8:
                yearsInWords = "вісім";
                break;
            case 9:
                yearsInWords = "дев'ять";
                break;
        }
    }
    else if (document.getElementById('roku').value >= 10 && document.getElementById('roku').value < 20) {
        switch (parseInt(document.getElementById('roku').value)) {
            case 10:
                yearsInWords = "десять";
                break;
            case 11:
                yearsInWords = "одинадцять";
                break;
            case 12:
                yearsInWords = "дванадцять";
                break;
            case 13:
                yearsInWords = "тринадцять";
                break;
            case 14:
                yearsInWords = "чотирнадцять";
                break;
            case 15:
                yearsInWords = "п'ятнадцять";
                break;
            case 16:
                yearsInWords = "шістандцять";
                break;
            case 17:
                yearsInWords = "сімнадцять";
                break;
            case 18:
                yearsInWords = "вісімнадцять";
                break;
            case 19:
                yearsInWords = "дев'ятнадцять";
                break;
        }
    }
    else if (document.getElementById('roku').value >= 20) {
        switch (parseInt(document.getElementById('roku').value / 10)) {
            case 2:
                yearsInWords = "двадцять";
                break;
            case 3:
                yearsInWords = "тридцять";
                break;
            case 4:
                yearsInWords = "сорок";
                break;
            case 5:
                yearsInWords = "п'ятдесят";
                break;
            case 6:
                yearsInWords = "шістдесят";
                break;
        }
        switch (document.getElementById('roku').value % 10) {
            case 1:
                yearsInWords += " один";
                break;
            case 2:
                yearsInWords += " два";
                break;
            case 3:
                yearsInWords += " три";
                break;
            case 4:
                yearsInWords += " чотири";
                break;
            case 5:
                yearsInWords += " п'ять";
                break;
            case 6:
                yearsInWords += " шість";
                break;
            case 7:
                yearsInWords += " сім";
                break;
            case 8:
                yearsInWords += " вісім";
                break;
            case 9:
                yearsInWords += " дев'ять";
                break;
        }
    }
    switch (document.getElementById('roku').value % 10) {
        case 1: yearsInWords += " рік";
            break;
        case 2:
        case 3:
        case 4: yearsInWords += " роки";
            break;
        case 0:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            yearsInWords += " років";
            break;
    }
    document.getElementById('yearsInWords').value = yearsInWords;
}
function task7() {
    if ((String(document.getElementById('num').value).length) < 2)
        alert('Завдання 7\nВведене число має бути двозначним');
    else
        document.getElementById('resdegree').value = (document.getElementById('num').value).charAt(1) ** document.getElementById('degree').value;
}